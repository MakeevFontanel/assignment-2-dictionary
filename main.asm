%define BUFFER_SIZE 255
%define	OFFSET 8

section .rodata
	overflow_err: db "Buffer overflow", 0
	key_not_found_err: db "Key is not found", 0
section .bss
	buffer:  times BUFFER_SIZE db 0

section .text
	
%include "colon.inc"	
%include "words.inc"

extern string_length
extern find_word
extern print_string
extern print_newline
extern read_word
extern exit

global _start

_start:
	mov	rdi , buffer
	mov	rsi , BUFFER_SIZE
	call	read_word 
	test	rax , rax
	je	.overflow
	mov	rdi , buffer
	mov	rsi , NEXT
	push	rdx		;save length of key
	call	find_word
	pop	rdx
	test	rax , rax
	je	.key_not_found
	add	rax , rdx 	; skipping id
	add	rax , OFFSET	; skipping key
	inc 	rax		; null-terminator
	mov	rdi , rax	
	call	print_string	; giving print_string pos of value
	call	print_newline
	call	exit
.overflow:
	mov 	rdi, overflow_err
	jmp 	.print_err
.key_not_found:
	mov 	rdi, key_not_found_err     
	jmp 	.print_err
.print_err:
	mov	rsi , rdi	
	call	string_length	; rsi untouched
	mov	rdx , rax	
	mov	rdi , 2		; stderr
	mov	rax , 1
	syscall
	call	print_newline
	call	exit
