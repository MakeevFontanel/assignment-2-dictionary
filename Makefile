ASM		= nasm
ASMFLAGS 	=-felf64
LD		= ld
main:	main.o dict.o lib.o
	$(LD) -o $@ $^

%.o:	%.asm
	$(ASM) $(ASMFLAGS) -o $@ $<



main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean

clean:
	rm -f dict.o main.o lib.o 
