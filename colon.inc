%define	NEXT 0

%macro colon 2
%ifid	%2
	%2: dq 	NEXT
	%define NEXT %2
%else	
	%error "The second argument must be id"
%endif

%ifstr	%1
	db	%1 , 0
%else	
	%error	"The first argument must be string"
%endif	
%endmacro
