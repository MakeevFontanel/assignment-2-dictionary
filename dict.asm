%define	OFFSET	8
global	find_word
extern  string_equals
extern	string_length

;input	rdi - pointer to null-term string , rsi - pointer to dictionary
find_word:
	add	rsi , OFFSET 	; skip id
	push	rsi
	push	rdi
	call	string_equals
	pop	rdi
	pop	rsi
	sub	rsi , OFFSET 	; returning to id pos
	cmp	rax , 1	 	
	je	.word_found
	mov	rsi , [rsi]	; getting pointer to next element
	test rsi , rsi		; if element points to 0, it is last element
	jne	find_word
	ret			; rax already zeroed
.word_found:
	mov	rax , rsi
	ret
